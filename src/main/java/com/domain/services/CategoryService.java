package com.domain.services;

import java.util.Optional;

import javax.transaction.TransactionScoped;

import com.domain.entity.Category;
import com.domain.repository.CategoryRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@TransactionScoped
public class CategoryService {
    @Autowired
    private CategoryRepository CategoryRepository;

    public Category save(Category category)
    {
        return CategoryRepository.save(category);
    }

    public Category findOne(Long id)
    {
        Optional<Category> category = CategoryRepository.findById(id);
        if(!category.isPresent())
        {
            return null;
        }

        return category.get();
    }

    public Iterable<Category> findAll()
    {
        return CategoryRepository.findAll();
    }

    public void removeOne(Long id)
    {
        CategoryRepository.deleteById(id);
    }
}
