package com.domain.services;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import com.domain.dto.ProductData;
import com.domain.entity.Product;
import com.domain.entity.Supplier;
import com.domain.repository.SupplierRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Transactional
public class SupplierService {
    @Autowired
    private SupplierRepository SupplierRepository;

    public Supplier save(Supplier Supplier)
    {
        return SupplierRepository.save(Supplier);
    }

    public Supplier findOne(Long id)
    {
        Optional<Supplier> Supplier = SupplierRepository.findById(id);
        if(!Supplier.isPresent())
        {
            return null;
        }

        return Supplier.get();
    }

    public Iterable<Supplier> findAll()
    {
        Iterable<Supplier> data2 = SupplierRepository.findAll();
        return data2;
    }

    public void removeOne(Long id)
    {
        SupplierRepository.deleteById(id);
    }
}
