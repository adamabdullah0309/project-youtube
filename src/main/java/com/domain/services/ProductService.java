package com.domain.services;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import com.domain.entity.Product;
import com.domain.entity.Supplier;
import com.domain.repository.ProductRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Transactional
public class ProductService {
    @Autowired
    private ProductRepository ProductRepository; 

    public Product create(Product product)
    {
        return ProductRepository.save(product);
    }
    
    public Product findOne(Long id)
    {
        Optional<Product> temp = ProductRepository.findById(id);
        if(!temp.isPresent())
        {
            return null;
        }
        return temp.get();
    }

    public Iterable<Product>  findAll()
    {
        return ProductRepository.findAll();
    }

    public void removeOne(Long id)
    {
        ProductRepository.deleteById(id);
    }

    public List<Product> findByName(String name)
    {
        return ProductRepository.findByNameContains(name);
    }

    public void addSupplier(Supplier supplier, Long productId)
    {
        Product product = findOne(productId);
        if(product == null)
        {
            throw new RuntimeException("Product with ID : "+productId+" not found");
        }
        product.getSuppliers().add(supplier);
        create(product);
    }
}
