package com.domain.repository;

import com.domain.entity.Supplier;

import org.springframework.data.repository.CrudRepository;

public interface SupplierRepository extends CrudRepository<Supplier, Long> {
    
}
