package com.domain.repository;

import java.util.List;

import com.domain.entity.Product;

import org.springframework.data.repository.CrudRepository;

public interface ProductRepository extends CrudRepository<Product, Long> {
    List<Product> findByNameContains(String name);
}
