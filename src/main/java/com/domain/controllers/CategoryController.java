package com.domain.controllers;

import javax.validation.Valid;

import com.domain.dto.CategoryData;
import com.domain.dto.ResponseData;
import com.domain.entity.Category;
import com.domain.services.CategoryService;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.validation.Errors;

@RestController
@RequestMapping("/api/categories")
public class CategoryController {
    @Autowired
    private CategoryService CategoryService;

    @Autowired
    private ModelMapper ModelMapper; 

    @PostMapping
    public ResponseEntity<ResponseData<Category>> create(@Valid @RequestBody CategoryData categoryData, Errors errors)
    {
        ResponseData<Category> responseData = new ResponseData<>();
        if(errors.hasErrors())
        {
            for (ObjectError error : errors.getAllErrors()) {
                responseData.getMessages().add(error.getDefaultMessage());
            }
            responseData.setStatus(false);
            responseData.setPayload(null);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(responseData);
        }
        Category category = ModelMapper.map(categoryData, Category.class);
        

        responseData.setStatus(true);
        responseData.setPayload(CategoryService.save(category));
        return ResponseEntity.ok(responseData);
    }

    @GetMapping
    public Iterable<Category> findAll()
    {
        return CategoryService.findAll();
    }
    
    @GetMapping("/{id}")
    public Category findOne(@PathVariable("id") Long id)
    {
        return CategoryService.findOne(id);
    }

    @PutMapping
    public ResponseEntity<ResponseData<Category>> update(@Valid @RequestBody CategoryData categoryData, Errors errors)
    {
        ResponseData<Category> responseData = new ResponseData<>();
        if(errors.hasErrors())
        {
            for (ObjectError error : errors.getAllErrors()) {
                responseData.getMessages().add(error.getDefaultMessage());
            }
            responseData.setStatus(false);
            responseData.setPayload(null);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(responseData);
        }
        Category category = ModelMapper.map(categoryData, Category.class);
        

        responseData.setStatus(true);
        responseData.setPayload(CategoryService.save(category));
        return ResponseEntity.ok(responseData);
    }
}
