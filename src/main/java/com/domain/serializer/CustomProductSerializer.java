package com.domain.serializer;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Set;

import com.domain.entity.Product;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;

public class CustomProductSerializer extends StdSerializer<Set<Product>> {
     public CustomProductSerializer() {
        this(null);
    }

    public CustomProductSerializer(Class<Set<Product>> t) {
        super(t);
    }

    @Override
    public void serialize(
            Set<Product> students,
            JsonGenerator generator,
            SerializerProvider provider)
            throws IOException, JsonProcessingException {

        Set<Product> prod = (Set<Product>) new ArrayList();
        for (Product s : students) {
            s.setSuppliers(null);
            prod.add(s);
        }
        generator.writeObject(prod);
    }
}
